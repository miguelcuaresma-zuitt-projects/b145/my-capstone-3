import React, { useState, useEffect } from "react";
import { Container, Table, Button, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import EditItemModal from "./EditItemModal";
import Swal from "sweetalert2";
function useForceUpdate(){
  const [val, setVal] = useState(0)
  return () => setVal(val => val + 1)
}
const Items = ({ props, productId, fetchData }) => {
  console.log(props.isActive);
  console.log(fetchData);
  // const productId = props._id
  const [count, setCount] = useState(0)
  const forceUpdate = useForceUpdate();
  
  const openAdd = () => setShowAdd(true);
  const closeAdd = () => setShowAdd(false);

  const [showEdit, setShowEdit] = useState(false);
  const [showAdd, setShowAdd] = useState(false);

  console.log(productId);
  let token = localStorage.getItem("token");

  

  const handleClose = () => setShowEdit(false);

  const archiveProduct = (e, productId) => {
    fetch(`https://damp-brushlands-99424.herokuapp.com/products/${productId}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          fetchData();
          
          Swal.fire({
            title: 'Archive successful',
            icon: 'success',
            text: 'Product now set to inactive'
          })
          

        } else {
          Swal.fire({
            title: 'Error occurred',
            icon: 'error',
            text: 'Please try again later'
          })

        }
      });
  };

  const activateProduct = (e, productId) => {
    fetch(`https://damp-brushlands-99424.herokuapp.com/products/${productId}/setActive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          fetchData();
          Swal.fire({
            title: 'Set Active successful',
            icon: 'success',
            text: 'Product now set to active'
          })
          
        } else {
          Swal.fire({
            title: 'Error Occurred',
            icon: 'error',
            text: 'Please try again later'
          })
        }
      });
  };
  
  return (
    <>
      <div>
        <table>
          <thead>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Stocks</th>
            <th>isActive</th>
          </thead>
          <tbody>
            <tr key={productId}>
              <td>{props.name}</td>
              <td>{props.description}</td>
              <td>{props.price}</td>
              <td>{props.countInStock}</td>
              <td>{String(props.isActive)}</td>
              <td>
                <Button
                  className="btn mr-3 mb-2"
                  variant="outline-dark"
                  onClick={() => setShowEdit(true)}
                >
                  Edit
                </Button>
              </td>
              {props.isActive ? (
                <td>
                  <Button 
                  className="btn ml-3 mr-3 mb-2"
                  variant="danger"
                  onClick={(e) => archiveProduct(e, productId)}>
                    Remove
                  </Button>
                </td>
              ) : (
                <td>
                  <Button 
                  className="btn ml-3 mr-3 mb-2"
                  variant="success"
                  onClick={(e) => activateProduct(e, productId)}>
                    Set Active
                  </Button>
                </td>
              )}
            </tr>
          </tbody>
        </table>
      </div>
      <EditItemModal
        show={showEdit}
        onHide={handleClose}
        productId={productId}
      />
    </>
  );
};

export default Items;
